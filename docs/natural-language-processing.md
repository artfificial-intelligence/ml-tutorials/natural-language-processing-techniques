# 자연어 처리 기법

## <a name="intro"></a> 개요
자연어 처리(NLP)는 컴퓨터와 인간 언어 사이의 상호 작용을 다루는 인공 지능의 한 분야이다. NLP는 컴퓨터가 자연어 텍스트와 음성을 이해, 분석, 생성 및 조작할 수 있도록 한다. NLP는 정보 검색, 기계 번역, 감정 분석, 질문 답변, 텍스트 요약, 텍스트 생성 등 다양한 영역에서 많은 응용을 가지고 있다.

이 포스팅에서는 다양한 기술과 도구를 사용하여 일반적인 자연어 처리 작업을 수행하는 방법을 설명한다.

- 텍스트 데이터를 추가 분석에 적합하도록 전처리한다.
- bag-of-words, 용어 빈도 역 문서 빈도(term frequency-inverse document frequency, TF-IDF), 단어 임베딩 등 다양한 방법을 사용하여 텍스트 데이터를 표현한다.
- 텍스트 데이터를 스팸 또는 햄, 긍정 또는 부정 또는 뉴스 주제와 같은 다양한 범주로 분류한다.
- 본문 자료를 요약하여 요점을 추출하고 길이를 줄인다.
- 처음부터 또는 캡션, 헤드라인 또는 스토리와 같은 일부 입력을 기반으로 텍스트 데이터를 생성한다.
- 기계 번역 모델을 사용하여 한 언어에서 다른 언어로 텍스트 데이터를 번역한다.
- 감정 분석 모형을 활용하여 텍스트 데이터의 감정을 분석한다.
- 질문 답변 모델을 사용하여 텍스트 데이터를 기반으로 질문에 답변한다.

이 포스팅을 읽고나면 자연어 처리의 기본 개념과 기법, 그리고 이를 자신의 프로젝트에 적용하는 방법에 대해 확실하게 이해할 수 있을 것이다.

시작합시다!

## <a name="sec_02"></a> 텍스트 전처리
텍스트 전처리는 추가 분석을 위해 원시 텍스트 데이터를 보다 적합하고 표준화된 형태로 변환하는 과정이다. 텍스트 전처리는 후속 작업의 품질과 정확성을 향상시킬 수 있기 때문에 자연어 처리에서 필수적인 단계이다. 텍스트 전처리는 일반적으로 다음과 같은 단계를 수반한다.

- **토큰화(tokenization)**: 텍스트를 단어, 문장 또는 문자와 같이 더 작은 단위로 분할하는 과정이다. 토큰화는 문장 부호, 공백 또는 특수 기호와 같은 관련 없는 요소로부터 텍스트의 의미 있는 요소를 분리하는 데 도움이 된다.
- **정규화(normalization)**: 텍스트를 소문자화, 스테밍(stemming), 레마티제이션(lemmatization) 또는 불용어 제거와 같이 균일하고 일관된 형태로 변환하는 과정이다. 정규화는 텍스트의 가변성과 복잡성을 감소시킬 뿐만 아니라 노이즈와 중복성을 제거하는 데 도움이 된다.
- **클리닝(cleaning)**: 이는 철자 오류, 오타, HTML 태그 또는 텍스트가 아닌 요소와 같은 텍스트의 원하지 않거나 부정확한 부분을 제거하거나 수정하는 과정이다. 클리닝은 잠재적인 오류나 오해를 피할 뿐만 아니라 텍스트의 가독성과 명확성을 높이는 데 도움이 된다.

NLTK를 사용한 토큰화:

```python
import nltk
from nltk.tokenize import word_tokenize, sent_tokenize

text = "Tokenization is the process of splitting the text into smaller units, such as words, sentences, or characters."
words = word_tokenize(text)
sentences = sent_tokenize(text)
print("Words:", words)
print("Sentences:", sentences)
```

NLTK(소문자화)를 사용한 정규화:

```python
normalized_text = [word.lower() for word in words]
print("Normalized text:", normalized_text)
```

NLTK를 사용한 청소(불용어 제거):

```python
from nltk.corpus import stopwords

stop_words = set(stopwords.words('english'))
cleaned_text = [word for word in normalized_text if word not in stop_words]
print("Cleaned text:", cleaned_text)
```

## <a name="sec_03"></a> 텍스트 표현
텍스트 표현(Text Representation)은 텍스트 데이터를 기계 학습 모델이나 알고리즘이 사용할 수 있는 수치 또는 기호 형태로 변환하는 과정이다. 텍스트 표현은 자연어 처리의 중요한 단계로서, 컴퓨터가 텍스트 데이터에 대한 수학적 연산과 계산을 수행할 수 있게 해준다. 텍스트 표현은 일반적으로 다음과 같은 방법을 포함한다.

- **bag-of-wors**: 이는 텍스트 표현의 가장 간단하고 일반적인 방법이다. 텍스트 데이터에 있는 모든 고유한 단어의 어휘를 만들고 각 단어에 고유한 인덱스를 할당하는 것을 포함한다. 그러면 각 텍스트 문서는 어휘와 동일한 길이의 벡터로 표현되며, 여기서 각 요소는 문서에 포함된 단어의 빈도 또는 존재에 해당한다. bag-of-words를 구현하고 이해하기 쉽지만 단어의 순서와 맥락을 무시하고 희소화한 고차원적인 벡터를 생성하는 등 몇 가지 한계를 가지고 있다.
- **용어 빈도-역 문서 빈도(TF-IDF)**: 문서와 말뭉치에서의 중요도에 따라 각 단어에 가중치를 부여하는 bag-of-word 방식의 확장이다. 용어 빈도(TF)는 단어가 문서에 나타나는 횟수이고, 역 문서 빈도(IDF)는 단어가 포함된 문서 수에 대한 전체 문서 수의 비율에 로그를 취한 것이다. TF-IDF 가중치는 TF와 IDF 값의 곱으로, 단어가 문서에서 얼마나 관련성이 높고 구별되는지를 반영한다. TF-IDF는 공통적이고 관련성이 없는 단어의 영향을 줄이고, 더 의미 있고 유익한 벡터를 생성하는 데 도움이 된다.
- **단어 임베딩(word embedding)**: 이는 어휘의 각 단어에 대한 저차원적이고 밀도가 높은 벡터를 학습하는 것을 포함하는 텍스트 표현의 더 발전되고 정교한 방법이다. 단어 임베딩은 신경망 또는 word2vec, GloVe 또는 fastText와 같은 다른 알고리즘을 사용하여 많은 텍스트 말뭉치로부터 학습된다. 단어 임베딩은 단어 간의 의미론과 구문론적 관계뿐만 아니라 문맥과 사용을 포착한다. 단어 임베딩은 ag-of-words과 TF-IDF의 한계를 극복하고 더 표현력 있고 강력한 벡터를 생성하는 데 도움이 된다.

scikit-learn을 사용한 bag-of-words:

```python
from sklearn.feature_extraction.text import CountVectorizer

corpus = [
    'This is the first document.',
    'This document is the second document.',
    'And this is the third one.',
    'Is this the first document?',
]
vectorizer = CountVectorizer()
X = vectorizer.fit_transform(corpus)
print("Bag-of-Words matrix:")
print(X.toarray())
print("Vocabulary:", vectorizer.get_feature_names())
```

scikit-learn을 사용한 TF-IDF:

```python
from sklearn.feature_extraction.text import TfidfVectorizer

tfidf_vectorizer = TfidfVectorizer()
X_tfidf = tfidf_vectorizer.fit_transform(corpus)
print("TF-IDF matrix:")
print(X_tfidf.toarray())
```

## <a name="sec_04"></a> 텍스트 분류
텍스트 분류는 텍스트 문서의 내용이나 메타데이터를 기반으로 텍스트 문서에 하나 이상의 레이블을 할당하는 과정이다. 텍스트 분류는 대규모 텍스트 데이터 모음을 정리, 필터링 및 분석하는 데 도움을 줄 수 있기 때문에 자연어 처리에서 일반적이고 유용한 작업이다. 텍스트 분류는 스팸 탐지, 감성 분석, 주제 모델링, 문서 분류 등 다양한 영역에서 많은 응용이 가능하다.

이 절에서는 Python과 scikit-learn, TensorFlow, PyTorch와 같은 라이브러리를 사용하여 텍스트 분류를 수행하는 방법을 보인다.

다음은 로지스틱 회귀 분류기를 사용하여 scikit-learn을 사용한 텍스트 분류의 예이다.

```python
from sklearn.datasets import fetch_20newsgroups
from sklearn.feature_extraction.text import CountVectorizer
from sklearn.model_selection import train_test_split
from sklearn.linear_model import LogisticRegression
from sklearn.metrics import classification_report, confusion_matrix
# Load the 20 newsgroups dataset
categories = ['alt.atheism', 'soc.religion.christian', 'comp.graphics', 'sci.med']
twenty_train = fetch_20newsgroups(subset='train', categories=categories, shuffle=True, random_state=42)
# Vectorize the text data
vectorizer = CountVectorizer()
X_train = vectorizer.fit_transform(twenty_train.data)
y_train = twenty_train.target
# Split the dataset into training and testing sets
X_train, X_test, y_train, y_test = train_test_split(X_train, y_train, test_size=0.3, random_state=42)
# Train a logistic regression classifier
clf = LogisticRegression(max_iter=1000)
clf.fit(X_train, y_train)
# Evaluate the classifier
y_pred = clf.predict(X_test)
print("Classification Report:")
print(classification_report(y_test, y_pred, target_names=twenty_train.target_names))
print("Confusion Matrix:")
print(confusion_matrix(y_test, y_pred))
```

위 코드는 로지스틱 회귀 분류기를 사용하여 20개 뉴스 그룹 데이터세트에 대해 텍스트 분류를 수행하는 방법을 보여준다. scikit-learn을 사용하여 텍스트 데이터를 벡터화하고 데이터세트를 훈련과 테스트 세트로 분할하며 분류기를 훈련하고 정확도, 정밀도, 리콜 및 F1-score와 같은 메트릭을 사용하여 성능을 평가한다.

## <a name="sec_05"></a> 텍스트 요약
텍스트 요약은 원본 문서의 요점과 정보를 보존하는 긴 텍스트 문서의 짧고 간결한 버전을 만드는 과정이다. 텍스트 요약은 시간, 공간 및 자원을 절약하고 텍스트 데이터의 가독성과 이해력을 향상시키는 데 도움을 줄 수 있기 때문에 자연어 처리에서 유용한 작업이다. 텍스트 요약은 뉴스 기사, 연구 논문, 리뷰, 이메일 등 다양한 영역에서 많은 응용을 가지고 있다.

이 섹션에서는 Python과 NLTK, gensim, Transformers와 같은 라이브러리를 사용하여 텍스트 요약을 수행하는 방법을 보인다.

다음은 NLTK를 이용한 추출 요약의 예이다.

```python
from nltk.tokenize import word_tokenize
from nltk.corpus import stopwords
from nltk.cluster.util import cosine_distance
from nltk import sent_tokenize

# Assume text is already defined
def sentence_similarity(sent1, sent2, stopwords=None):
    if stopwords is None:
        stopwords = []
    
    words1 = [word.lower() for word in sent1 if word not in stopwords]
    words2 = [word.lower() for word in sent2 if word not in stopwords]
    
    all_words = list(set(words1 + words2))
    
    vector1 = [0] * len(all_words)
    vector2 = [0] * len(all_words)
    
    for word in words1:
        vector1[all_words.index(word)] += 1
    
    for word in words2:
        vector2[all_words.index(word)] += 1
    
    return 1 - cosine_distance(vector1, vector2)

def build_similarity_matrix(sentences, stopwords=None):
    if stopwords is None:
        stopwords = []
    
    similarity_matrix = np.zeros((len(sentences), len(sentences)))
    
    for idx1 in range(len(sentences)):
        for idx2 in range(len(sentences)):
            if idx1 == idx2:
                continue
            similarity_matrix[idx1][idx2] = sentence_similarity(sentences[idx1], sentences[idx2], stopwords)
    
    return similarity_matrix

def generate_summary(text, top_n=5):
    stop_words = set(stopwords.words("english"))
    summarize_text = []

    sentences = sent_tokenize(text)
    sentence_similarity_martix = build_similarity_matrix(sentences, stop_words)
    sentence_similarity_graph = nx.from_numpy_array(sentence_similarity_martix)
    scores = nx.pagerank(sentence_similarity_graph)

    ranked_sentence = sorted(((scores[i],s) for i,s in enumerate(sentences)), reverse=True)    

    for i in range(top_n):
        summarize_text.append("".join(ranked_sentence[i][1]))

    return " ".join(summarize_text)

summary = generate_summary(text)
print("Summary:", summary)
```

## <a name="sec_06"></a> 텍스트 생성
텍스트 생성은 프롬프트, 키워드, 컨텍스트 또는 이전 텍스트와 같은 일부 입력에 기초하거나 처음부터 새로운 텍스트 데이터를 생성하는 과정이다. 텍스트 생성은 주어진 요구 사항이나 기대 사항을 충족시킬 수 있는 일관되고 관련성 있으며 다양한 텍스트 데이터를 생성할 수 있는 능력을 필요로 하기 때문에 자연어 처리에서 도전적이고 창의적인 작업이다. 텍스트 생성은 캡션, 헤드라인, 스토리, 가사, 농담 등 다양한 영역에서 많은 응용을 가지고 있다.

다음은 NLTK를 이용한 규칙 기반 생성의 예이다.

```python
import nltk
from nltk import CFG
from nltk.parse.generate import generate

grammar = CFG.fromstring("""
    S -> NP VP
    NP -> Det N
    VP -> V NP
    Det -> 'the' | 'a'
    N -> 'cat' | 'dog'
    V -> 'chased' | 'ate'
""")
for sentence in generate(grammar, n=5):
    print(' '.join(sentence))
```

## <a name="sec_07"></a> 텍스트 번역
텍스트 번역은 원문의 의미와 양식을 보존하면서 텍스트 데이터를 한 자연어에서 다른 자연어로 변환하는 과정이다. 텍스트 번역은 언어 간의 언어적, 문화적, 맥락적 차이를 설명할 수 있는 자연어 텍스트를 이해하고 생성할 수 있는 능력을 필요로 하기 때문에 자연어 처리에서 어렵고 복잡한 작업이다. 텍스트 번역은 의사소통, 교육, 비즈니스, 관광 등 다양한 영역에서 많은 응용이 있다.

다음은 NLTK를 이용한 규칙 기반 번역의 예이다.

```python
from nltk.tokenize import word_tokenize

def translate_sentence(sentence, translation_table):
    words = word_tokenize(sentence)
    translated_words = [translation_table.get(word, word) for word in words]
    return ' '.join(translated_words)
english_to_french = {
    'apple': 'pomme',
    'orange': 'orange',
    'banana': 'banane'
}
sentence = "I like apples and oranges."
translated_sentence = translate_sentence(sentence, english_to_french)
print("Translated sentence:", translated_sentence)
```

## <a name="sec_08"></a> 텍스트 감성 분석
텍스트 감성 분석은 텍스트 문서나 텍스트 세그먼트의 긍정, 부정, 중립 등 감정 톤과 태도를 파악하고 추출하는 과정이다. 텍스트 감성 분석은 텍스트 작성자 또는 텍스트 주체의 의견, 느낌, 선호도를 이해하는 데 도움을 줄 수 있어 자연어 처리에 유용한 작업이다. 텍스트 감성 분석은 제품 리뷰, 소셜 미디어 게시물, 고객 피드백 등 다양한 영역에서 많은 응용이 이루어지고 있다.

다음은 텍스트블롭을 이용한 어휘 기반 정서 분석의 예이다.

```python
from textblob import TextBlob

def analyze_sentiment(text):
    blob = TextBlob(text)
    sentiment = blob.sentiment.polarity
    if sentiment > 0:
        return "positive"
    elif sentiment < 0:
        return "negative"
    else:
        return "neutral"
text = "I love this product! It's amazing."
sentiment = analyze_sentiment(text)
print("Sentiment:", sentiment)
```

## <a name="sec_09"></a> 텍스트 질문 답변
텍스트 질문 답변은 텍스트 문서나 텍스트 모음에서 자연어 질문에 대한 답을 찾아내고 추출하는 과정이다. 텍스트 질문 답변은 자연어 텍스트와 질문을 이해하고 분석할 수 있을 뿐만 아니라 관련되고 정확한 답을 검색하고 생성할 수 있는 능력을 필요로 하기 때문에 자연어 처리에서 도전적이고 유용한 작업이다. 텍스트 질문 답변은 교육, 연구, 고객 서비스 등 다양한 영역에서 많은 응용이 가능하다.

다음은 spaCy를 이용한 규칙 기반 질문 답변의 예이다.

```python
import spacy

nlp = spacy.load("en_core_web_sm")
def answer_question(question, text):
    doc = nlp(text)
    question_doc = nlp(question)
    for sent in doc.sents:
        if question_doc[0].lemma_ in [token.lemma_ for token in sent]:
            return sent.text
text = "Apple is a fruit. It is red or green in color."
question = "What is Apple?"
answer = answer_question(question, text)
print("Answer:", answer)
```

## <a name="summary"></a> 마치며
자연어 처리 기술에 대한 이 포스팅을 완료했다. Python과 라이브러리들을 사용하여 일반적이고 유용한 자연어 처리 작업을 수행하는 방법을 설명하였다. 또한 목표를 달성하기 위해 다양한 타입의 방법, 모델을 사용하는 방법과 결과를 평가하고 개선하는 방법도 보였다.

다음은 이 포스팅에서 설명한 내용을 요약한 것이다.

- **텍스트 전처리**: 추가 분석에 적합하도록 텍스트 데이터를 토큰화하고 정규화하고 정리하는 방법
- **텍스트 표현**: bag-of-words, TF-IDF, 단어 임베딩 등의 방법을 사용하여 텍스트 데이터를 숫자 또는 기호 벡터로 변환하는 방법
- **텍스트 분류**: naive Bayes, 지원 벡터 머신, 신경망 등의 방법을 사용하여 텍스트 데이터를 여러 범주에 할당하는 방법
- **텍스트 요약**: 추출과 추상적 요약같은 방법을 사용하여 주요 요점을 추출하고 텍스트 데이터의 길이를 줄이는 방법
- **텍스트 생성**: 규칙 기반, 통계, 신경 및 하이브리드 생성과 같은 방법을 사용하여 처음부터 또는 일부 입력을 기반으로 새로운 텍스트 데이터를 만드는 방법
- **텍스트 번역**: 규칙 기반, 통계, 신경 및 하이브리드 번역과 같은 방법을 사용하여 텍스트 데이터를 한 자연어에서 다른 자연어로 변환하는 방법
- **텍스트 감정 분석**: 어휘 기반, 규칙 기반, 통계, 신경, 하이브리드 분석 등의 방법을 사용하여 텍스트 데이터의 감정 톤과 태도를 파악하고 추출하는 방법
- **텍스트 질문 답변**: 규칙 기반, 통계, 신경 및 하이브리드 답변과 같은 방법을 사용하여 텍스트 데이터에서 자연어 질문에 대한 답변을 찾고 추출하는 방법

<span style="color:red">예 코드 미실행</span>
