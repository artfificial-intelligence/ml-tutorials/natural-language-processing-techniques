# 자연어 처리 기법 <sup>[1](#footnote_1)</sup>

> <font size="3">다양한 기법을 활용하여 자연어 처리 작업을 수행하는 방법을 알아본다.</font>

## 목차

1. [개요](./natural-language-processing.md#intro)
1. [텍스트 전처리](./natural-language-processing.md#sec_02)
1. [텍스트 표현](./natural-language-processing.md#sec_03)
1. [텍스트 분류](./natural-language-processing.md#sec_04)
1. [텍스트 요약](./natural-language-processing.md#sec_05)
1. [텍스트 생성](./natural-language-processing.md#sec_06)
1. [텍스트 번역](./natural-language-processing.md#sec_07)
1. [텍스트 감성 분석](./natural-language-processing.md#sec_08)
1. [텍스트 질문 답변](./natural-language-processing.md#sec_09)
1. [마치며](./natural-language-processing.md#summary)

<a name="footnote_1">1</a>: [ML Tutorial 25 — Natural Language Processing Techniques](https://ai.plainenglish.io/ml-tutorial-25-natural-language-processing-techniques-7500ee6f7212?sk=7f7f5a59a351de7345861bed66addd68)를 편역하였습니다.
